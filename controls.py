import cv2
import numpy as np
np.set_printoptions(suppress=True)

from PySide2 import QtWidgets
from PySide2.QtCore import (QObject, Signal)

from extractor import HarrisDetector, Extractor
from matcher import Matcher
from labeledslider import LabeledSlider

class MatchingModel(QObject):
    valueChanged = Signal()
    def __init__(self, threshold, count, angle, winsize, parent=None):
        super().__init__(parent)
        dummy_image = np.zeros((480, 640)).astype('float32')
        self.__old_frame = (list(), list(), dummy_image, dummy_image)
        self.__threshold = threshold
        self.__count = count
        self.__angle = angle
        self.__winsize = winsize
        self.__match_color = (1, 0, 0)
        self.__point_color = (1, 0, 1)

    def apply(self, frame):
        (old_kps, old_des, old_gray, old_original) = self.__old_frame
        original = frame.copy()

        rotation = self.__get_rotation_matrix(original)
        inverse_rotation = self.__get_rotation_matrix(original, inv=True)

        to_show = self.__rotate(original, rotation)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = self.__rotate(gray, rotation)

        self.hd = HarrisDetector(adaptive=True, threshold=self.__threshold, win_size=self.__winsize, n=self.__count)
        self.ex = Extractor()
        self.matcher = Matcher()

        # Detect points and match
        kps = self.hd.detect(gray)
        des = self.ex.compute(gray.copy(), kps)
        matches = self.matcher.match(old_des, des)
        image = cv2.drawMatches(old_original, old_kps, to_show, kps, matches, None, flags=0, matchColor=self.__match_color, singlePointColor=self.__point_color)

        # Make next frame faster by keeping already computed keypoints, but
        # since they have been modified by transformation, we need to bring back
        # to real position.
        # This way we don't compute the keypoints twice per frame
        if matches:
            self.__untransform_keypoints(kps, inverse_rotation)

        self.__old_frame = (kps, des, gray, frame)
        return image

    def __untransform_keypoints(self, kps, inverse_rotation):
        """ Disgusting but 'it just works' """
        ys, xs = zip(*[ np.array(kp.pt) for kp in kps ])
        merged = np.dstack((ys, xs))
        inversed = cv2.transform(merged, inverse_rotation)
        old_kps = list(map(tuple, inversed[0]))
        for (i, kp) in enumerate(kps):
            x, y = old_kps[i]
            kp.pt = (int(x), int(y))
        return kps

    def __rotate(self, image, rotation):
        height, width = image.shape[:2]
        rotated = cv2.warpAffine(image, rotation, (width, height))
        return rotated

    def __get_rotation_matrix(self, image, inv=False):
        height, width = image.shape[:2]
        point = (width//2, height//2)
        angle = self.__angle if inv else -self.__angle
        return cv2.getRotationMatrix2D(point, angle, scale=1)

    def threshold(self):
        return self.__threshold

    def angle(self):
        return self.__angle

    def count(self):
        return self.__count

    def winsize(self):
        return self.__winsize

    def setThreshold(self, value):
        if self.__threshold == value:
            return
        self.__threshold = value
        self.valueChanged.emit()

    def setAngle(self, value):
        if self.__angle == value:
            return
        self.__angle = value
        self.valueChanged.emit()

    def setCount(self, value):
        if self.__count == int(value):
            return
        self.__count = int(value)
        self.valueChanged.emit()

    def setWindowSize(self, value):
        if self.__winsize == int(value):
            return
        self.__winsize = int(value)
        self.valueChanged.emit()

class ControlsView(QtWidgets.QWidget):
    valueChanged = Signal()
    def __init__(self, parent=None):
        super().__init__(parent)
        self.__model = None

        layout = QtWidgets.QVBoxLayout()


        self.__threshold = LabeledSlider("Threshold: ", value=10.0/255.0, minimum=0.001, maximum=0.5, steps=0.001)
        self.__winsize   = LabeledSlider("Window Size: ", minimum=1, maximum=31, steps=2.0)
        self.__angle     = LabeledSlider("Angle: ", minimum=0.0, maximum=360.0)
        self.__count     = LabeledSlider("Count: ", value=30, minimum=5.0, maximum=200.0)

        layout.addWidget(self.__threshold)
        layout.addWidget(self.__winsize)
        layout.addWidget(self.__angle)
        layout.addWidget(self.__count)

        self.setLayout(layout)

    def setModel(self, model):
        if self.__model:
            self.__threshold.valueChanged.disconnect(self.__model.setThreshold)
            self.__winsize.valueChanged.disconnect(self.__model.setWindowSize)
            self.__angle.valueChanged.disconnect(self.__model.setAngle)
            self.__count.valueChanged.disconnect(self.__model.setCount)
            self.__model.valueChanged.disconnect(self.valueChanged)

        self.__model = model

        self.__threshold.setValue(self.__model.threshold())
        self.__winsize.setValue(self.__model.winsize())
        self.__angle.setValue(self.__model.angle())
        self.__count.setValue(self.__model.count())

        self.__threshold.valueChanged.connect(self.__model.setThreshold)
        self.__winsize.valueChanged.connect(self.__model.setWindowSize)
        self.__angle.valueChanged.connect(self.__model.setAngle)
        self.__count.valueChanged.connect(self.__model.setCount)
        self.__model.valueChanged.connect(self.valueChanged)
