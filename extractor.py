import sys
import cv2
import numpy as np
import math
np.set_printoptions(suppress=True)

# In the Harris corner detector, every pixel has its Harris matrix `H`. To
# avoid looping over every pixel, we compute the determinants and the trace
# (and everything else) using basic operations.
#
# The H matrix is composed of 4 (really 3) values, I_x^2, I_y^2 and I_xy.

# Helpers
def to_degree(radian):
    offset = 0 if radian > 0 else 360
    return (radian * 180.0 / np.pi) + offset

def gaussian_weight():
    # TODO FIXME: The SIFT paper seems to talk about a Gaussian weight for
    # the magnitude
    gx = cv2.getGaussianKernel(16, sigma=4)
    return gx * gx.transpose()

def compute_orientations(i_x, i_y):
    return np.arctan2(i_y, i_x)

def compute_magnitudes(i_x2, i_y2):
    return np.hypot(i_x2, i_y2)

def reverse(a):
    return a[::-1]

def swap(t):
    (a, b) = t
    return (b, a)

def non_zero_iter(a):
    ys, xs = a.nonzero()
    merged = np.dstack((ys, xs))
    return merged[0].tolist()

def gradient(image):
    return gradient_x(image), gradient_y(image)

def gradient_x(image):
    image = cv2.Sobel(image, cv2.CV_32F, 1, 0, ksize=3, scale=1.0)
    return cv2.GaussianBlur(image, (3,3), 0)

def gradient_y(image):
    image = cv2.Sobel(image, cv2.CV_32F, 0, 1, ksize=3, scale=1.0)
    return cv2.GaussianBlur(image, (3,3), 0)

def harris_components(i_x, i_y):
    """
    Compute the three components that make up Harris matrices.

    We also apply a light gaussian blur.
    """

    def weight(size):
        gx = cv2.getGaussianKernel(size, sigma=1.5)
        return gx * gx.transpose()

    i_xy = i_x * i_y
    i_x2 = i_x ** 2
    i_y2 = i_y ** 2
    w = weight(3)
    i_x2 = cv2.filter2D(i_x2, -1, w)
    i_y2 = cv2.filter2D(i_y2, -1, w)
    i_xy = cv2.filter2D(i_xy, -1, w)
    return i_x2, i_y2, i_xy

def determinants(i_x2, i_y2, i_xy):
    """ Computes all determinants of all `H` matrix """
    ad = i_x2 * i_y2
    bc = i_xy ** 2
    return ad - bc

def traces(i_x2, i_y2):
    """ The trace of a matrix is simply the sum of the diagonal """
    return i_x2 + i_y2

def harmonic_mean(determinants, traces):
    """
    Compute the harmonic mean using the determinants and the traces

    The harmonic mean is simply det(H)/trace(H)
    """
    with np.errstate(divide='ignore', invalid='ignore'):
        return np.nan_to_num(determinants / traces)

def fast_local_maximum(array):
    """ 
    Find the local maximum around 3x3 neighborhood (set the other to 0)

    This technique is fast because it does not depend on the number of keypoints,
    instead, it only depends on the window size (fixed to 3 here)

    This is based from https://stackoverflow.com/questions/3986345/how-to-find-the-local-minima-of-a-smooth-multidimensional-array-in-numpy-efficie#answer-3986476

    This is about ~ 3x faster than the other approach with Yosemite image
    """
    top    = np.roll(array, 1, 0)
    bottom = np.roll(array, -1, 0)
    left   = np.roll(array, 1, 1)
    right  = np.roll(array, -1, 1)
    top_right = np.roll(top, -1, 1)
    top_left = np.roll(top, 1, 1)
    bottom_right = np.roll(bottom, -1, 1)
    bottom_left = np.roll(bottom, 1, 1)
    predicate_cross = ((array >= top) & (array >= bottom) & 
                       (array >= left) & (array >= right))
    predicate_diag = ((array >= top_right) & (array >= top_left) & 
                       (array >= bottom_right) & (array >= bottom_left))
    predicate = predicate_cross & predicate_diag
    return array * np.where(predicate, 1, 0)

def find_minimum_radius(f_xj, x, y, f_xi, ys, xs):
    """
    From the following formula:
        r = min |x_i - x_j| st. f(x_i) < C_robust * f(x_j) forall x_j
            where x_j  are all points (coordinates)
                  f_xj are all values from harris matrix
    """

    # f_xj is sorted previously so we can do binary search
    idx = np.searchsorted(f_xj, f_xi)

    # All values after (including) idx satisfies the formula's predicate
    all_minimums_x = xs[idx:] - x
    all_minimums_y = ys[idx:] - y
    norms = np.hypot(all_minimums_x, all_minimums_y)
    if norms.size > 0:
        return np.min(norms)
    else:
        return sys.float_info.max

def anms(harris):
    """
    Adaptive non-maximum suppression

    We find the minimum radius of every keypoints from the current harris matrix
    We skip values that are 0 for performance reason.
    """
    harris = harris.copy()
    indices = np.indices(harris.shape)
    robustness = 0.9 # The MOPS paper recommends a robustness of 0.9
    f_xj = harris * robustness

    # By sorting once, we are able to then quickly find the index of the first
    # value above the robustness * f_xj. From that index to the end of the array
    # are all the values that satisfies the predicate of the radius formula
    # from the MOPS paper. (See find_minimum_radius())
    # (We don't have to run a predicate over the NxM image, so it's much faster.
    # and the search is then in NlogN time)
    ys, xs = np.unravel_index(np.argsort(f_xj, axis=None), f_xj.shape)
    sorted_f_xj = f_xj[ys, xs]

    for (y, x) in non_zero_iter(harris):
        f_xi = harris[y, x]
        radius = find_minimum_radius(sorted_f_xj, x, y, f_xi, ys, xs)
        harris[y, x] = radius
    return harris

class HarrisDetector:
    def __init__(self, threshold=10.0/255.0, n=500, adaptive=True, win_size=3):
        self.threshold = threshold
        self.n = n
        self.adaptive = adaptive
        self.win_size = win_size

    def detect(self, image):
        i_x, i_y = gradient(image)
        i_x2, i_y2, i_xy = harris_components(i_x, i_y)
        dets = determinants(i_x2, i_y2, i_xy)
        trace = traces(i_x2, i_y2)
        responses = harmonic_mean(dets, trace)
        suppressed = self.__suppress(responses)

        orientations = compute_orientations(i_x, i_y)
        magnitudes = compute_magnitudes(i_x2, i_y2)
        kps = self.__to_keypoints(suppressed, orientations, magnitudes)
        return kps

    def __to_keypoints(self, responses, orientations, magnitudes):
        if self.adaptive:
            dim = responses.shape
            sorted = responses.argsort(axis=None)
            ys, xs = np.unravel_index(reverse(sorted), dim)
            responses = responses[ys, xs].reshape(dim)
            orientations = orientations[ys, xs].reshape(dim)
            magnitudes = magnitudes[ys, xs].reshape(dim)
            predicate = responses > 0
            ys = ys.reshape(dim)
            xs = xs.reshape(dim)
            merged = np.dstack((responses, ys, xs, orientations, magnitudes))
            merged = merged[predicate].tolist()[:self.n]
            kps = []
            for [response, y, x, angle, size] in merged:
                kp = cv2.KeyPoint(x, y, _size=size, _angle=angle, _response=response)
                kps.append(kp)
            return kps

        else:
            predicate = responses > 0
            ys, xs = np.indices(responses.shape[:2])
            merged = np.dstack((responses, ys, xs, orientations, magnitudes))
            merged = merged[predicate].tolist()
            kps = []
            for [response, y, x, angle, size] in merged:
                kp = cv2.KeyPoint(x, y, _size=size, _angle=angle, _response=response)
                kps.append(kp)
            return kps

    def __suppress(self, rs):
        """ 
        rs: responses 
        output: list of cv2.Keypoints
        """

        height, width = rs.shape[:2]
        _, thresholded = cv2.threshold(rs, self.threshold, 0, type=cv2.THRESH_TOZERO)
        local_suppressed = self.__lnms(thresholded)
        if self.adaptive:
            radii = anms(local_suppressed)
            return radii
        else:
            return local_suppressed

    def __lnms(self, rs):
        """
        Perform local non-maximum suppression

        It suppresses (sets to zero) values that are not the maximum around a square
        window with radius win_size//2
        """

        def zero_border(image, win_size):
            half_size = max(32, win_size//2)
            image[:half_size, :]  = 0
            image[:, :half_size]  = 0
            image[-half_size:, :] = 0
            image[:, -half_size:] = 0
            return image

        def is_local_maximum(image, x, y, win_size):
            half_size = win_size//2
            start_y, end_y = y - half_size, y + half_size + 1
            start_x, end_x = x - half_size, x + half_size + 1
            return image[y, x] == np.max(image[start_y:end_y, start_x:end_x])

        # We make a copy because we're going to be modifying the values
        rs = rs.copy()
        height, width = rs.shape[:2]
        rs = zero_border(rs, self.win_size)
        if self.win_size == 3:
            return fast_local_maximum(rs)
        else:
            for (y, x) in non_zero_iter(rs):
                if not is_local_maximum(rs, x, y, self.win_size):
                    rs[y, x] = 0
        return rs

class Extractor:
    def __init__(self):
        self.gauss_weight = gaussian_weight()
        self.patch_size = 8
        self.debug = False
        self.count = 0

    def compute(self, image, keypoints):
        """ returns descriptors """
        return [self.describe(image, kp) for kp in keypoints]

    def describe(self, image, keypoint):
        x, y = tuple(map(int, keypoint.pt))
        angle = keypoint.angle
        half_diag = math.ceil(self.patch_size * math.sqrt(2))
        patch = image[y-half_diag:y+half_diag, x-half_diag:x+half_diag]
        pt = (half_diag, half_diag)
        canonical = self.canonical_rotate(patch, angle, pt)
        orientations, norms = self.get_orientation_and_norm_patch(canonical, pt)

        # We create the bins once instead of creating it 16 times. This increases
        # performance dramatically because a lot of time was spent creating this
        # from the number of bins and the range.
        xedges = np.linspace(-np.pi, np.pi, 9)
        yedges = np.arange(17)
        yd = np.zeros_like(orientations)
        bins = (xedges, yedges)
        i = 0
        for col in range(4):
            off_y = col * 4
            for row in range(4):
                off_x = row * 4
                yd[off_y:off_y+4,off_x:off_x+4] = i
                i += 1

        flat = orientations.flatten()
        weights = norms.flatten()
        yd = yd.flatten()
        hist, _, _ = np.histogram2d(flat, yd, bins=bins, weights=weights)
        vector = hist.T

        vector = self.normalize(vector.flatten())
        return vector

    def canonical_rotate(self, image, angle, pt):
        angle = to_degree(angle)
        rotation = cv2.getRotationMatrix2D(pt, angle, scale=1)
        rotated = cv2.warpAffine(image, rotation, swap(image.shape[0:2]))
        return rotated

    def get_orientation_and_norm_patch(self, rotated, pt):
        i_x, i_y = gradient(rotated)
        i_x2, i_y2, i_xy = harris_components(i_x, i_y)
        patch_x  = self.get_patch(i_x, pt)
        patch_y  = self.get_patch(i_y, pt)
        patch_x2 = self.get_patch(i_x2, pt)
        patch_y2 = self.get_patch(i_y2, pt)
        patch_orientations = compute_orientations(patch_x, patch_y)
        patch_norms = compute_magnitudes(patch_x2, patch_y2) * self.gauss_weight

        if self.debug and self.count < 4:
            a = np.concatenate((patch_x, patch_y), axis=1)
            b = np.concatenate((patch_x2, patch_y2), axis=1)
            c = np.concatenate((patch_orientations, patch_norms), axis=1)
            all = np.concatenate((a, b, c), axis=0)
            all = cv2.resize(all, (0, 0), fx=8, fy=8, interpolation=cv2.INTER_NEAREST)
            cv2.imshow(f'{kp.pt}', all)
            self.count += 1

        return patch_orientations, patch_norms

    def normalize(self, vector):
        """ 
        Normalize and threshold the vector from the SIFT paper 

        The first normalization is to improve robustness againts contrast 
        changes ('Invariance to affine changes in illumination')

        The thresholding is used to prioritize orientations distributions instead
        of prioritizing large magnitudes

        Finally we normalize to unit length again
        """
        vector = vector / np.linalg.norm(vector)
        vector = np.clip(vector, 0, 0.2)
        vector = vector / np.linalg.norm(vector)
        return vector

    def get_patch(self, image, pt):
        (x, y) = map(int, pt)
        a_y = y - self.patch_size
        b_y = y + self.patch_size
        a_x = x - self.patch_size
        b_x = x + self.patch_size
        return image[a_y:b_y, a_x:b_x]
