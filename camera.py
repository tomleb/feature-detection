import cv2

from PySide2 import QtCore
from PySide2.QtCore import (Qt, QObject, QThread, QTimer, Signal, Slot)

class Container(QObject):
    """ An immutable container that is used to pass data through signals """
    def __init__(self, value, parent=None):
        super().__init__(parent)
        self.__value = value

    @property
    def value(self):
        return self.__value

    def delete(self):
        """ Need to call delete when we're done, otherwise we get a memory leak """
        del(self.__value)

class Camera(QThread):
    """ 
    The camera periodically send the new frame

    We can access the new frame with container.value. Don't forget to container.delete()
    """
    frameUpdated = Signal(QObject)

    def __init__(self, fps=30.0, parent=None):
        super().__init__(parent)
        self.__cap = cv2.VideoCapture()
        self.__timer = QTimer(self)
        self.__timer.timeout.connect(self.updateFrame)
        self.__fps = fps

    def startTimer(self):
        self.__cap.open(0)
        self.__timer.start(1000.0/self.__fps)
        self.__timer.setSingleShot(False)

    def updateFrame(self):
        ret, frame = self.__cap.read()
        if not ret:
            return

        # We're using opencv to get the current frame (and this is in BGR
        # instead of RGB) so we convert it
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        container = Container(frame, self)
        self.frameUpdated.emit(container)
