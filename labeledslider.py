from PySide2 import QtCore, QtWidgets
from PySide2.QtCore import (Qt, QObject, QThread, QTimer, Signal, Slot)
from PySide2.QtWidgets import QLabel

from precisionslider import PrecisionSlider

class LabeledSlider(QtWidgets.QWidget):
    """ Slider with a Label, duh! """
    valueChanged = Signal(float)
    def __init__(self, text, parent=None, **kwargs):
        super().__init__(parent)
        layout = QtWidgets.QHBoxLayout()

        label = QLabel(text)
        self.__value_label = QLabel()
        self.__slider = PrecisionSlider(**kwargs)
        self.__slider.valueChanged.connect(self.__setFromSlider)

        layout.addWidget(label)
        layout.addWidget(self.__value_label)
        layout.addWidget(self.__slider)
        self.setLayout(layout)
        self.setValue(0)

    def __setFromSlider(self, value):
        self.__value_label.setText(f'{value:.3f}')
        self.valueChanged.emit(value)

    @Slot()
    def setValue(self, value):
        if self.__slider.value() == value:
            return

        self.__slider.setValue(value)
