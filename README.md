# Feature Detection and Matching

Implemented Harris corner detection with adaptive non-maximum suppression from
[MOPS](https://www.microsoft.com/en-us/research/publication/multi-scale-oriented-patches)

Each point is described using SIFT descriptor from the [SIFT
paper](https://www.cs.ubc.ca/~lowe/papers/ijcv04.pdf)

Matching is done naively in O(n^2) and it uses a ratio test between the best and
second best sum of squared differences (SSD).

The repository includes a Qt-based GUI and a simple cli application. See usage
for detail.

## Installing dependencies

```bash
$ pip install -r requirements.txt
```

## Usage

### CLI

```bash
$ python pictures/Yosemite1.jpg pictures/Yosemite2.jpg
```

### GUI

The GUI uses Qt and opens the webcam.

```bash
$ python gui.py
```

## Implementation

The relevant files for feature detections and matchings are extractor.py and
matcher.py.

The file extractor.py includes the HarrisDetector class that is used to find
interesting keypoints in the images. By default, it uses local non-maximal
suppression and adaptive non-maximal suppression.

This file also includes the Extractor class that is used to compute the
SIFT descriptor of each keypoints given. Each keypoints is given a 128-dimension
vector as its value. See SIFT paper for detail.

The matcher.py file implements a naive n^2 SSD comparison. The best and second
best matches are kept to perform a ratio test to eliminate ambiguous matches.

## Screenshots

![GUI](screenshots/gui.png)

![CLI](screenshots/cli.png)

## Requirements

Requires python 3 with opencv and numpy.

Also requires Qt if using the GUI.

## References

* [SIFT](https://www.cs.ubc.ca/~lowe/papers/ijcv04.pdf)

* [MOPS](https://www.microsoft.com/en-us/research/publication/multi-scale-oriented-patches)
