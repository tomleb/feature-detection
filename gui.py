#!/usr/bin/env python

import os
import sys
import cv2
import numpy as np
np.set_printoptions(suppress=True)

from PySide2 import QtWidgets
from PySide2.QtGui import QPixmap
from PySide2.QtWidgets import QLabel

from labeledslider import LabeledSlider
from camera import Camera
from controls import (ControlsView, MatchingModel)

class MainWindow(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.__camera = Camera(parent=self)
        self.__camera.frameUpdated.connect(self.updateFrame)

        layout = QtWidgets.QVBoxLayout()
        images = QtWidgets.QHBoxLayout()

        self.__filtered = QLabel()
        self.__filtered.resize(640*2, 480)
        images.addWidget(self.__filtered)
        layout.addLayout(images)

        self.__model = MatchingModel(10.0/255.0, 20, 20.0, 3.0)
        self.__controls = ControlsView(self)
        self.__controls.setModel(self.__model)

        layout.addWidget(self.__controls)

        self.setWindowTitle('Feature Detection and Matching')
        self.setLayout(layout)

        self.__camera.startTimer()

    def updateFrame(self, container):
        frame = container.value
        height, width, chan = frame.shape

        frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        frame = self.__tofloat32(frame)
        
        filtered = self.__model.apply(frame)
        filtered = self.__touint8(filtered)

        status, buf = cv2.imencode('.ppm', filtered)
        pixmap = QPixmap()
        pixmap.loadFromData(buf.tostring())
        self.__filtered.setPixmap(pixmap)
        container.delete()

    def __tofloat32(self, img):
        img = img.astype(np.float32)
        img = img / 255.0
        return img

    def __touint8(self, img):
        img = img * 255
        img = np.clip(img, 0, 255)
        img = img.astype(np.uint8)
        return img

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    # Because PySide2 does not have qSetMessagePattern yet
    if not os.environ.get('QT_MESSAGE_PATTERN'):
        os.environ['QT_MESSAGE_PATTERN'] = '[%{type}] %{appname} (%{file}:%{line}) - %{message}'

    widget = MainWindow()
    widget.show()
    sys.exit(app.exec_())
