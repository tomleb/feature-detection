from PySide2 import (QtCore, QtWidgets)
from PySide2.QtCore import Qt
from PySide2.QtWidgets import (QLabel, QSlider)

class PrecisionSlider(QtWidgets.QWidget):
    """ Slider that allows decimal steps """
    valueChanged = QtCore.Signal(float)
    def __init__(self, value=None, minimum=0.0, maximum=100.0, steps=1.0, parent=None):
        super().__init__(parent)

        if value is None:
            value = minimum

        self.__steps = steps
        slider_max = (maximum - minimum) / steps

        self.__minimum = minimum
        self.__slider = QSlider(Qt.Horizontal, minimum=0, maximum=slider_max)
        self.__slider.valueChanged.connect(self.__setFromSlider)
        self.__slider.setValue(self.__mapFromPrecision(value))

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.__slider)
        self.setLayout(layout)

    def __setFromSlider(self, value):
        pvalue = self.__mapToPrecision(value)
        self.valueChanged.emit(pvalue)

    def setValue(self, pvalue):
        value = self.__mapFromPrecision(pvalue)
        if value == self.__slider.value():
            return

        self.__slider.setValue(value)

    def value(self):
        return self.__mapToPrecision(self.__slider.value())

    def __mapFromPrecision(self, value):
        return (value - self.__minimum) / self.__steps

    def __mapToPrecision(self, value):
        return self.__steps * value + self.__minimum
