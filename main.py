import cv2
import numpy as np
np.set_printoptions(suppress=True)
import argparse
import itertools
import os
import sys

from extractor import HarrisDetector, Extractor
from matcher import Matcher

def benchmark():
    import cProfile
    # cProfile.run('matcher.match(des, des)', sort='tottime')
    # cProfile.run('load_detect_mark_show(\'yosemite/Yosemite1.jpg\')', sort='tottime')
    # cProfile.run('hd.detect(gray)', sort='tottime')
    cProfile.run('ex.compute(gray, kps)', sort='tottime')
    sys.exit(0)

def load_image(filename):
    original = np.float32(cv2.imread(filename))/255.0
    gray = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)
    return original, gray

def load_detect_mark_show(hd, ex, filename, contrast=1):
    print(f'Processing {filename}')
    win_size = 3
    original, gray = load_image(filename)
    original /= contrast
    gray /= contrast

    kps = hd.detect(gray)
    des = ex.compute(gray, kps)

    print('Finished')
    return original, kps, des

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Detect and match points in two images')
    parser.add_argument('images', metavar='IMAGE', help='Path to image', nargs=2)
    parser.add_argument('--threshold', default=10.0/255.0, help='Threshold for keypoints elimination')
    parser.add_argument('--adaptive', default=True, help='Use adaptive non-maximum suppression')
    parser.add_argument('--count', default=500, help='Number of points to keep')
    args = parser.parse_args()

    adaptive  = args.adaptive
    count     = int(args.count)
    threshold = float(args.threshold)
    file1    = args.images[0]
    file2    = args.images[1]

    hd = HarrisDetector(adaptive=adaptive, threshold=threshold, n=count)
    ex = Extractor()
    image1, kps1, des1 = load_detect_mark_show(hd, ex, file1)
    image2, kps2, des2 = load_detect_mark_show(hd, ex, file2)

    print('Beginning matching')
    matcher = Matcher()
    matches = matcher.match(des1, des2)
    image = cv2.drawMatches(image1, kps1, image2, kps2, matches, None, flags=0, matchColor=(1, 0, 0), singlePointColor=(0, 1, 0))
    print('Finished')

    cv2.imshow('matches', image)

    while (True):
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    cv2.destroyAllWindows()
