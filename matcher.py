import sys
import cv2
import numpy as np

class Matcher:
    def __init__(self, threshold=0.75, ratio=0.6):
        self.threshold = threshold
        self.ratio = ratio

    def match(self, des1, des2):
        if len(des2) < 2:
            return []

        matches = []
        scores = {}
        for (i, d1) in enumerate(des1):
            squared_diff = (des2 - d1)**2
            distances = np.sum(squared_diff, axis=1)
            best_idx, second_best_idx, *_ = np.argpartition(distances, 1)
            best = distances[best_idx]
            second_best = distances[second_best_idx]
            ratio = best / second_best

            if best < self.threshold and ratio < self.ratio:
                match = cv2.DMatch(i, best_idx, 1, ratio)
                matches.append(match)
        return matches

    def distance_between_descriptors(self, d1, d2):
        return np.sum((d1 - d2) ** 2)
